
Groups
==========================================

Groups are collections of templates, documents and users (members).  You can have as many templates, documents and users
as you like in a group.

If you are using the API to send out lots of contracts under one group, or you have a lot of users that you want to
cover within one group, then check your activity falls within the throttling limits for the plan
the API user is signed up to.


group/
~~~~~~~~~~~~~~~~~~~~
Return list of groups the API user belongs to.

Allowed methods: GET, POST

Example of a GET response in JSON::

  {
  "meta": {
    "limit": 20, 
    "next": null, 
    "offset": 0, 
    "previous": null, 
    "total_count": 14
  }, 
  "objects": [
    {
      "created": "2013-01-04T10:30:19",
      "is_active": true,
      "modified": "2013-01-04T10:30:19", 
      "name": "My Group", 
      "resource_uri": "/api/v1/group/mygroup/", 
      "slug": "mygroup"
    }, 
    {
      "created": "2013-01-11T17:47:29", 
      "is_active": true,
      "modified": "2013-01-11T17:47:32", 
      "name": "My Group 1", 
      "resource_uri": "/api/v1/group/mygroup1/", 
      "slug": "mygroup1"
    }, 
    ]
    }
    

If you haven't reached your account limit on groups, you can create more::

    curl --dump-header - -H "Content-Type: application/json"
    -H "Authorization: ApiKey f6009234a674463d8d2def328b4321:a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0"
    -X POST --data '{ "name": "Group on" }' https:/legalesign.com/api/v1/group/


If you attempt to add more groups than your account is limited to you'll receive a 400 response with
the following data::

    {
    "group": {
    "plan": "Please upgrade to add more groups, or deactivate a current group to add another"
    }


There is no DELETE for groups. If you have a redundant group set its is_active attribute to false. You'll
be able to add groups to replace them.



group/<group_slug>/
~~~~~~~~~~~~~~~~~~~

Return a group details, including members.

Allowed methods: GET, PATCH

Example of a GET response in JSON::
    
     {
    "created": "2012-11-10T14:09:48", 
    "default_email": "",
    "default_extraemail": "", 
    "footer": "<p>Legalesign Limited, Appleton Tower</p>", 
    "footer_height": 110, 
    "header": null, 
    "header_height": null,
    "is_active": true, 
      "members": [
        "/api/v1/user/f6009234a674463d8d2def328b4321/", 
        "/api/v1/user/ded3690f83104b959382d8812da58b/"
      ], 
      "modified": "2013-01-11T17:11:07", 
      "name": "My Group", 
      "pagesize": 1,  //PDF page size, 1 = A4 (only available currently)
      "resource_uri": "/api/v1/group/mygroup/", 
      "slug": "mygroup"
    } 


Attributes available for PATCH

================== ======= =================================================
Attribute          Type    Notes
================== ======= =================================================
name               string  maxlength 60, minlength 3
footer             string  html for text at the footer of each PDF page
default_email      string  maxlength 1000
default_extraemail string  maxlength 2000
footer_height      integer pixel height for footer on pdf, 1px = 0.025cm
header             string  html for text at the head of each PDF page
header_height      integer pixel height for header on pdf, 1px = 0.025cm
is_active          bool    shut down/turn on a group.
pagesize           integer must be 1, more options coming soon
================== ======= =================================================

Once header and footer are saved to a group you can use them in documents by using
the keyword 'default' in the header and footer attributes when creating a new document.

Example of a PATCH in curl::

    curl --dump-header - -H "Content-Type: application/json"  -H
    "Authorization: ApiKey f6009234a674463d8d2def328b4321:a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0"
    -X PATCH --data '{ "name":"New name", "default_email": "A new default email" }'
    https://legalesign.com/api/v1/group/mygroup/
    
N.B. Since this is a PATCH, only the attributes sent to the server are updated. If this was sent
as a PUT request it would have also updated the other attributes that are not sent to null.