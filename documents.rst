
Documents
==========================================

Documents are objects that have been sent out to be signed.


document/
~~~~~~~~~~~~~~~~~~~~
Return documents that have not been archived or send a document to be signed.

Allowed methods: GET, POST

A GET request automatically filters archived documents. To get
all documents add `?archived=all&...` to the querystring or specify `?archived=true` for the
archive. The list can also be filtered by group url name `?group=mygroup1&...` or group resource_uri
`?group=/api/v1/group/mygroup1/` and by status `?status=30&...` (show only signed docs).


Example of a GET response in JSON::

    {
    "meta": {
    "limit": 20, 
    "next": "/api/v1/document/?username=f6009234a674463d8d2def328b4321&api_key=a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0&limit=20&offset=20&format=json", 
    "offset": 0, 
    "previous": null, 
    "total_count": 49
    }, 
    "objects": [
    {
      "archived": false, 
      "created": "2013-02-15T11:58:34", 
      "group": "/api/v1/group/mygroup1/", 
      "modified": "2013-03-19T12:26:38", 
      "name": "Doc 1",
      "download_final": true,   //bool indicates final PDF is available for download
      "resource_uri": "/api/v1/document/51361087-ce3c-4eae-bb26-d77712fd9374/",
      "signers": [
        [
            "api/v1/signer/2c7f7ac4-7226-4608-b2aa-765fca2473f5", 
            "firstname", 
            "lastname", 
            "[email]",
            "behalf of",
            true, //whether this signer has fields to complete
            40,   //signer status 
            0     //signer order (zero-indexed)
        ]
      ], 
      "status": 30,  #document status
      "user": "/api/v1/user/f6009234a674463d8d2def328b4321/", 
      "uuid": "51361087-ce3c-4eae-bb26-d77712fd9374"
    }, 
    {
      "archived": false, 
      "created": "2013-02-17T08:51:37", 
      "group": "/api/v1/group/mygroup2/", 
      "modified": "2013-02-17T08:51:37", 
      "name": "Doc 2",
      "download_final": false,
      "resource_uri": "/api/v1/document/88f24b34-d051-4893-8d3e-70373af6c262/",
      "signers": [
        [
            "api/v1/signer/2c7f7ac4-7226-4608-b2aa-765fca2473f5",
            "firstname", 
            "lastname", 
            "[email]", 
            "behalf of",
            false, //whether this signer has fields to complete
            10,   //signer status 
            0     //signer order (zero-indexed)
        ]
      ], 
      "status": 10, #document status
      "user": "/api/v1/user/f6009234a674463d8d2def328b4321/", 
      "uuid": "88f24b34-d051-4893-8d3e-70373af6c262"
    },
    ...
    
.. _document_status_codes:

Document status codes are as follows:

====== ========================
Status Explanation
====== ========================
10     Sent
20     Fields completed
30     Signed
40     Removed (before signing)
====== ========================

.. _signer_status_codes:

Signer status codes are as follows:

====== ====================
Status Explanation
====== ====================
5      Scheduled to be sent 
10     Sent
15     Email opened
20     Visited
30     Fields completed
40     Signed
50     Downloaded
====== ====================


A POST request will send out a new document to signers.
POST attributes:

=================== ============ ======== =======================================================================================================
Attribute           Type         Required Details
=================== ============ ======== =======================================================================================================
group               resource_uri Y        The group you're using to send the document
name                string       Y        Name of document, max length 60
signers             list         Y        See below. A list of signer dictionary objects
template*           resource_uri N*       Text/HTML document to send (already saved to site).
text*               string       N*       Full HTML of doc.
templatepdf*        resource_uri N*       PDF document to send (already saved to site).
append_pdf          bool         N        Default=false. Append Legalesign validation info to final PDF. If not included uses the group default.
auto_archive        bool         N        Default=true. Send to archive a few hours after signing*. Keeps web interface clutter free.
do_email            bool         N        Default=false. If true will use Legalesign email to send notification emails
footer              string       N        The footer for the final pdf. Use keyword 'default' to use group's default footer
footer_height       integer      N        Pixel height of PDF footer, if used. 1px = 0.025cm
header              string       N        The header for the final pdf. Use keyword 'default' to use group's default header
header_height       integer      N        Pixel height of PDF header if used. 1px = 0.025cm
pdftext             dict         N        Sender text for pdf (if used), use element label for keys
return_signer_links bool         N        Default=false. Return document links for signers in the response BODY. Not recommended.
signature_placement integer      N        Default=1. 1=Foot of each page, 2=End of document
signature_type      integer      N        Default=4. 4=PDF Certification plus Legalesign Validation, 1=Legalesign validation only 
signers_in_order    bool         N        Default=false. Notify signers in their order sequence. If false all are notified simulataneously
user                resource_uri N        Will assign document to user with API key if omitted
=================== ============ ======== =======================================================================================================

* You must reference either a template, text, or templatepdf
* Sample template resource_uri: '/api/v1/template/<uid>/
* Sample template pdf resource_uri: '/api/v1/templatepdf/<uid>/
* <uid> is equivalent to the long number on the edit page URL.
* Auto archive will send to the web interface archive, storage archiving of the final file will occur approx. 14 days later.


* For more information on formatting text, see :ref:`general_fields`
* A group can have a ready-saved default footer, use 'default' as the footer value to choose this.
* If you set auto_archive to false, use the /archived/ endpoint to archive it after signing.

Signer attributes.
A signer should be a dictionary object with the following attributes

============= ======= ===================================================================
Attribute     Type    Notes
============= ======= ===================================================================
firstname     string  required, max length 60
lastname      string  required, max length 60
email         string  required, max length 75
behalfof      string  optional, max length 120
order         integer required, must be zero-indexed and sequential
message*      string  optional, use 'default' to use group's default message
extramessage* string  optional, use 'default' to use group's default extra message
============= ======= ===================================================================


* do_email in the POST request must be true in order to use message and extramessage
* Each group can have a default message and default 'extramessage', use the keyord 'default' to use them
* 'message' is text that is added to the email that is sent to the signer to tell them there is a document ready to be signed
* 'extramessage' is an additional plain text email, useful if you need to send more background information


 Example of a POST request in curl::
 
    curl --dump-header - -H "Authorization: ApiKey f6009234a674463d8d2def328b4321:
    a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0" -H "Content-Type: application/json"
    -X POST --data '{ "group": "/api/v1/group/mygroup/", 
    "name": "Name of doc", "template": "/api/v1/template/bba85862-ccaa-4ffe-99ed-d62cfd763f59/",
    "signers": [{"firstname": "Joe", "lastname": "Blogs",
    "email": "email@legalesign.com", "order": 0 }],
    "do_email": true}' https://legalesign.com/api/v1/document/

In this POST, a template is being called, there is 1 signer. Legalesign will send out the notification emails to signers.

If successful the response from your POST request will be a 201. It will contain the new resource_uri in the 'Location' header and
, if you set return_signer_links to true, the URL links for each signer in the BODY. If you are sending out notification
emails yourself you can use these signer links in your emails.

If the request fails you will receive a 400 and the BODY will contain an error report.

Example of a failed POST, where name is omitted::

    HTTP/1.0 400 BAD REQUEST
    Expires: -1
    Content-Type: application/json
    Pragma: no-cache
    Cache-Control: private, no-store, no-cache, must-revalidate, post-check=0, pre-check=0
    Connection: close
    Server:xxx
    Date: xxx
    {
        "document": {
          "name": [
            "This field is required."
          ]
        }
      }


Example of a successful POST. The document's status resource_uri is in the Location header::

    HTTP/1.0 201 CREATED
    Expires: -1
    Vary: Accept
    Location: https://legalesign.com/api/v1/status/4cfc13c2-894a-4f76-89a6-6be322debe33/
    Pragma: no-cache
    Cache-Control: private, no-store, no-cache, must-revalidate, post-check=0, pre-check=0
    Content-Type: application/json
    Connection: close
    Server: xxx
    Date: xxx
    "OK"


document/<document_uuid>/
~~~~~~~~~~~~~~~~~~~~~~~~~~~
Return a specific document details (extended)


Available methods: GET, DELETE


Some attributes explained:

=================== ======= =======================================================
Attribute           Type    Explanation
=================== ======= =======================================================
signature placement integer 1=place signature on each page, 2=place at end of doc
signature_type      integer 1=basic signature, 2=advanced signature
has_fields          bool    Whether or not the document has fields to complete
hash_value          string  The hash value of the final pdf
=================== ======= =======================================================


Example of a GET in JSON::

 {
  "archived": false, 
  "created": "2013-03-25T15:54:11", 
  "footer": "",
  "footer_height": null, 
  "group": "/api/v1/group/mygroup/", 
  "has_fields": true,
  "download_final": false,  //bool indicates final pdf is available to download
  "hash_value": null,  //the hash of the final pdf (when signed)
  "header": "", 
  "header_height": null, 
  "modified": "2013-03-25T15:54:54", 
  "name": "opt1", 
  "resource_uri": "/api/v1/document/71c3b1b1-8aa1-4719-89f6-0c7126c6830b/", 
  "sign_time": null, 
  "signature_placement": 2, //1 = place on each page, 2= place at end of doc
  "signature_type": 1, //1 = basic signature, 2 = advanced esignature
  "signers": [
    [
        "api/v1/signer/2c7f7ac4-7226-4608-b2aa-765fca2473f5",
        "firstname", 
        "lastname", 
        "[email]",
        "behalf of",
        true, //whether this signer has fields to complete
        30,   //signer status 
        0     //signer order (zero-indexed)
    ]
  ], 
  "status": 20, 
  "text": "<html>\n <head>\n </head>\n <body>\n  <p>\n   <span class=\"field\" data-auto=\"\" data-idx=\"0\" data-name=\"few\" data-optional=\"false\" data-options=\"\" data-signee=\"1\" data-state=\"1\" data-type=\"text\">\n    d\n   </span>\n</body>\n</html>", 
  "user": "/api/v1/user/f6009234a674463d8d2def328b4321/", 
  "uuid": "71c3b1b1-8aa1-4719-89f6-0c7126c6830b"
  }


Example of a DELETE in curl::
    curl --dump-header - -H "Authorization: ApiKey f6009234a674463d8d2def328b4321:
    a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0" -X DELETE
    http://legalesign.com/api/v1/document/71c3b1b1-8aa1-4719-89f6-0c7126c6830b/

Important note about DELETE for a document:

    DELETE does **not** remove the document permanently it marks the document status as 'removed'
    and archives the document.
    
    In the web interface deletion will trigger emails to the signers to
    tell them the document is void, these email are not triggered through this API call.
    You will need to send these emails through your own system.
    

archived/<document_uuid>/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
If you sent a document and turned off auto_archive you can archive (or unarchive it) via the API 
by sending a PATCH request with data archived=True/False.

This will only archive or unarchive the doc if it is signed. To remove and void a document
before it is signed use the DELETE method at the document/<uuid>/ endpoint.

Allowed methods: GET, PATCH

Example in curl::

     curl --dump-header - -H "Authorization: ApiKey f6009234a674463d8d2def328b4321:
     a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0" -H "Content-Type: application/json"
     -X PATCH --data '{ "archived": true }'
     https://legalesign.com/api/v1/archived/71c3b1b1-8aa1-4719-89f6-0c7126c6830b/
     

If the document is not signed you will get a 400 error code and the following error message::

    {
      "archived": {
        "status": [
          "Document cannot be archived unless it is signed"
        ]
    }
    
    
    
document/preview/
~~~~~~~~~~~~~~~~~~~~

Returns a redirect response (302) with link in the Location header to a one-use temporary URL you can redirect to, to
see a preview of the signing page. Follow the redirect immediately since it expires after a few seconds.

Allowed methods: POST

============ ============ ======== =============
Attribute    Type         Required Notes
============ ============ ======== =============
text         string       Y
title        string       Y
group        resource_uri Y
signee_count integer      N        defaults to 2
============ ============ ======== =============


status/
~~~~~~~~~~~~~~~~~~~
Return document status, whether archived and document group. Status
returns a shortened version of the /document/ endpoint.

Allowed methods: GET

Filters for unarchived documents by default. Add ?filter=false or ?filter=all for
archived or all documents.
Filter by group by adding a group parameter to the querystring. Use either the group url name or the group resource_uri, e.g. group=my-group or group=/api/v1/group/my-group/


For status codes see: :ref:`Document status codes <document_status_codes>`

Example GET response in JSON::

    {
      "meta": {
        "limit": 20, 
        "next": "/api/v1/status/?username=f6009234a674463d8d2def328b4321&api_key=a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0&limit=20&offset=20&format=json", 
        "offset": 0, 
        "previous": null, 
        "total_count": 64
      }, 
      "objects": [
        {
          "archived": false, 
          "resource_uri": "/api/v1/status/51361087-ce3c-4eae-bb26-d77712fd9374/", 
          "status": 30,
          "download_final": true,
        },
        ...
        
        
status/<document_uuid>/
~~~~~~~~~~~~~~~~~~~~~~~~~

Return single document status

Allowed methods: GET

Example GET response in JSON::

    {
    "archived": false, 
    "resource_uri": "/api/v1/status/51361087-ce3c-4eae-bb26-d77712fd9374/", 
    "status": 30,
    "download_final": true,
    }

For status codes see: :ref:`Document status codes <document_status_codes>`



signer/<signer_uuid>/
~~~~~~~~~~~~~~~~~~~~~~~

Return status and other details of an individual document signer. Each signer has
an associated user object (that contains name and email) and a document object. 'has_fields' indicates
whether the signer has fields to complete.

Allowed methods: GET

Example of a GET response in JSON::

    {
    "document": "/api/v1/document/51361087-ce3c-4eae-bb26-d77712fd9374/", 
    "email": "email@legalesign.com", 
    "first_name": "Archie", 
    "has_fields": true, 
    "last_name": "Gemmill", 
    "order": 0, 
    "resource_uri": "/api/v1/signer/03ca2bcf-ead1-483a-93da-201e41e8a651/", 
    "status": 40
    }


signer/<signer_uuid>/send-reminder/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Send a reminder email to a signer. Takes optional data 'text', a text string with a message for the
signer. HTML will be stripped. The reminder email includes a new link to the document for the signer.

Returns http response 200 'OK'.

Allowed methods:POST


signer/<signer_uuid>/reset/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
If a document has been forwarded use this endpoint to return the document to earlier signers.

Will return an error if the document has been signed, if the document's current signer email is the same
as the one sent, or if the email is not found to be an earlier signer. Explanatory text for errors will be
found the in the BODY of the response.

Allowed methods: POST

============ ============ ======== ================================================================================
Attribute    Type         Required Notes
============ ============ ======== ================================================================================
email        string       Y        email of earlier signer
notify       boolean      N        Default false. Email the current signer to say the document has been called back
============ ============ ======== ================================================================================

Returns http response 200 'OK'.

Allowed methods:POST


signer/<signer_uuid>/new-link/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Returns the document link for the signer in the response BODY. Useful if you
want to send your own reminder emails. Link is specific to this signer.

If the signer does not have an account with Legalesign this will be a 1-use link to the
document. For existing users it will be a link direct to the document
if the user is logged in or via the login page if not.

Returns http response 200.

Allowed methods:GET


   
pdf/<document_uuid>/
~~~~~~~~~~~~~~~~~~~~~~

Returns the raw PDF data for either the draft PDF if unsigned, or the
final PDF if signed.

Allowed methods: GET

Example of a GET request in curl::

    curl --dump-header - -H "Authorization: ApiKey f6009234a674463d8d2def328b4321:
    a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0" 
    -X GET -o finaldoc.pdf
    https://legalesign.com/api/v1/pdf/51361087-ce3c-4eae-bb26-d77712fd9374/
    

Once signed the /document/<uuid>/ endpoint will include the SHA-256 hash value (attribute 'hash_value')
for the signed pdf. You can use the hash value to ensure the downloaded document is not corrupted during delivery.

If you request an old PDF it may have gone into deep storage and not be immediately available. In this
case you will received a 202 HTTP code. A storage request will have been created and if you try
again after 5 hours it wil be available.  If you use the callbacks it will also include an entry when the document is
available: the resource_uri will be the relevant document, the name will be 'storage_retrieval' and the
value will be 'completed'.

pdf/preview/
~~~~~~~~~~~~~~

Returns raw data of PDF preview. Useful when preparing templates, documents, headers or footers.

Allowed methods: POST

===================== ============ ======== =============================================================
Attribute             Type         Required Description
===================== ============ ======== =============================================================
text                  string       Y        html of the document
signee_count          integer      Y        number of signers
signature_type        integer      Y        1=basic signature, 2=advanced signature, 3=no signatures
is_signature_per_page integer      Y        1=signatures on each page, 2=signatures at end of doc
group                 resource_uri Y        the relevant group (for the default header/footer if used)
title                 string       N        document title
header                string       N        html for header at top of each page
header_height         integer      N        required if using header, pixel height of header, 1px=0.025cm
footer_height         integer      N        required if using footer, pixel height of footer, 1px=0.025cm
pdfheader             bool         N        set to 'true' to use group default header and footer
===================== ============ ======== =============================================================



    
