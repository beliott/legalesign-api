
Members
==========================================

Add, list or remove members from groups.


member/
~~~~~~~~~~~~~~~~

Allowed methods: GET, POST


The GET method will return all the members of all the groups the API user belongs to.

Filter by group by adding a group parameter to the querystring. Use either the group url name or the group resource_uri, e.g. group=my-group or group=/api/v1/group/my-group/

Example of a GET response in JSON::

    {
    "meta": {
    "limit": 20, 
    "next": null, 
    "offset": 0, 
    "previous": null, 
    "total_count": 2
    }, 
    "objects": [
    {
      "created": "2013-01-04T10:33:10", 
      "group": "/api/v1/group/my-group/", 
      "modified": "2013-04-30T09:07:16", 
      "resource_uri": "/api/v1/members/b349cf86-d0b7-4f5e-aced-dcca90cd0b68/", 
      "user": "/api/v1/user/f6009234a674463d8d2def328b4321/"
    }, 
    {
      "created": "2013-04-09T12:00:40", 
      "group": "/api/v1/group/my-group/", 
      "modified": "2013-04-30T09:07:16", 
      "resource_uri": "/api/v1/members/9ecf411e-1ea7-4119-ac3b-2e1b37d9523e/", 
      "user": "/api/v1/user/ded3690f83104b959382d8812da58b/"
    }
    ]
    }
    
    
POST adds another person to a group, it should contain the group and email of person who will join.

If the person is not an existing user, it could be preferable to use the /user/ endpoint to create a new user and add the person
to your group(s) in one call.


========== ============ ======================================================================
Attributes Type         Description
========== ============ ======================================================================
group      resource_uri group you want person to join
email      email        email of person to join
do_email   bool         default=false, use legalesign to send email notification to new member
========== ============ ======================================================================


Example of POST in curl::

    curl --dump-header - -H "Content-Type: application/json"
    -H "Authorization: ApiKey f6009234a674463d8d2def328b4321:a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0"
    -X POST --data '{  "group": "/api/v1/group/bbj/",
    "email": "test@example.com", "do_email": true }'
    https://legalesign.com/api/v1/member/


If the person is already a member of Legalesign they will join the group immediately. If they
are not signed up already they will be 'invited'. See below for more information on /invited/ endpoint below.

Errors will be returned with a 400 response and a description of the error in the BODY.

For example, BODY of response if person has already joined::

    {
    "member": {
      "exists": "User is already a member of this group"
    }


BODY of response if person has already been invited (but not signed up to Legalesign yet)::

    {
    "member": {
    "invited": "User has been invited to join, but has not yet signed up"
    }


member/<uuid>
~~~~~~~~~~~~~~~~

Allowed methods: GET, DELETE

DELETE will remove the member from the group.
To add the person back to the group POST their email to the /member/ endpoint, as above.


invited/
~~~~~~~~~~~~~~~~~

List all people who have been invited to joined groups the API user belongs to. Invitations
are created when the person you add to a group has no account on Legalesign. Once they
create an account they will become a member of the group.

Filter by group by adding a group parameter to the querystring. Use either the group url name or the group resource_uri, e.g. group=my-group or group=/api/v1/group/my-group/


Allowed methods: GET

Example GET response in JSON::

    {
    "meta": {
    "limit": 20, 
    "next": null, 
    "offset": 0, 
    "previous": null, 
    "total_count": 1
    }, 
    "objects": [
    {
      "created": "2013-04-30T12:13:05", 
      "email": "test@example.com", 
      "group": "/api/v1/group/my-group/", 
      "resource_uri": "/api/v1/invited/ab3739c6-ba72-4c18-976b-2b09c88bb474/"
    }
    ]
    }


invited/<uuid>/
~~~~~~~~~~~~~~~~~

Allowed methods: DELETE

Remove the invitation. When the person signs in they will not be made a member of the group.
