
.. _document_callback:

Document Callback/Notifications
==========================================

Use this system to get notified when something happens on Legalesign. These do
not affect your API throttle limit.

Tell us a URL endpoint (must be https) and we will POST you changes in your groups.

There are two callback options, you can use either, or both.

1. Receive everything that has happened in the last 6 minutes, every 6 minutes - signer status changes and document status changes
2. Receive notification a document has been signed in real-time.

The second option is easier to implement. If you only need to know when something has been signed use the second option.
If you need more information - when a signer opens an email, or views a document - use the first option.

In either case, first tell us your URL endpoint, and the system you want to use. If you want to be able to subscribe/unsubscribe
to these notifications via the API you can do that too - it's not in the documentation but just ask for more information.


Option 1. Everything every 6 minutes.
==========================================

The data is POSTED in two sections 'data' and 'signed'. The data section is a JSON obj that is a
list of dictionaries of the changes. The dictionaries are a record of each change
to document and siger status in the order they occured. The second section is a signed string
(base64 encoded) you can verify
the data JSON obj is genuine should you wish.

The POSTed data will look like::

    {u'data': [u'[{"timestamp": "2013-04-21T13:47:31.822120",
    "name": "status", "value": "40",
    "resource_uri": "/api/v1/signer/5we2293df-1731-484c-9d3b-b10b68168d42/"},
    u'{"timestamp": "2013-04-21T13:47:32.624100",
    "name": "status", "value": "30",
    "resource_uri": "/api/v1/document/4a315cfe-1731-484c-9d3b-b10b68168d42/"}]'],
    u'signed': [u'R3F3R1FuOHVwaVpTT3VtOUl0dlFBTzdHMDBxeDVhSEV2MkkyeGF5LzVGWEJ0Y
    ytEYjdxQ3JyZWl0RnBHVU92UnRPaHljVGtxazExeXJ4N0t1Z0MvUk5UckM3S25HWmlTcXhha1ky
    OU4ra203YWdKV09YUFdYSk4rRXZ4T2lQc3FkU25KQzgzNEs2bkoyVWE5NUhJUW1CWUd4WU1iQjR
    2UFZvdnRQUXJtZlpRbTZvM1l6aFJpci9wWU0rQzdsaFh0RTBwaHBjOGVyM0t2cHMyTlV2V2VjW
    DV6OE81UlhtQmpkTVZjMFQzWU9rbzA0VTI5Mm0ySGJTbE5CVDd1WDZEMGdWN3UvYVQvMU1lcC9
    0U3JmWFVoUTBFdUFSazVEOWhHVFJmb3owQm41cURTSmxxU1JCdHhYa1JNVXBld1NDUnhSMitYV
    EZjUkdxUVl2bm1UZjQ1N2hNbXpvMnA2NnhJRTlMbmtubFJaYW1HNWVWV0NPT256RWhYY2JvRHR
    nOU04VFgvbTJWUUVqVklZU0QxRUpWdE83S2xpSzBLdjBvbG5JWTVHWTQwVU14aEIyeGpGY0JsN
    mJuK0J1OFBBcXFBTGFLTzRKK3pwcjRJUDJueU10Q0xWQXB4WC8xOFhSUDhMSE1jOW9QcUxIVnF
    3L0c2aW5uZFRQVlB6d3hjaTZnWldNcUtLdUQ5UXdrOFRiL2tObUgxNkN2VUh6M3BlSjd6TEdKd
    UdPazk1Vm5sNmcyV01zeGpvTS80citlSW0xRkpOYzVKU3B3ay9zWTNZcVdMaFNhT0ovV0dORHd
    SOUFWamZrYkQyKzN0dDR2WTh6U095ZHc5YjFHeUZWSVRtL0YzTmVpZVRFRDdqVThvak1zc0Rac
    k9waUVmaVRBV2NteWNKY3Njd25zNFZEbFE9']}


The data contains a list of dictionaries that contains the notification information.

============ ==========================================
Attributes   Explanation
============ ==========================================
timestamp    ISO format of when the change occured
resource_uri The object the notification relates to.
name         What the message relates to
value        The value the message relates to.
============ ==========================================

The POSTed message above tells you the signer "/api/v1/signer/5we2293df-1731-484c-9d3b-b10b68168d42/" changed to status 40 (signed) at
13:47:31 on 21st April, and that document '/api/v1/document/4a315cfe-1731-484c-9d3b-b10b68168d42/'
was fully signed (status 30) at 13:47:32 on 21st April. The dictionary will contain each status
change in order they occured with the timestamp, so it may contain multiple status changes for a single signer.


'status' - Document and Signer Status
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The majority of callback will have the name 'status' and these refer to status changes for
signers and documents. The codes for those status changes will be in value.

For document status codes see: :ref:`Document status codes <document_status_codes>`

For signer status codes see: :ref:`Signer status codes <signer_status_codes>`

The resource_uri refers to the relevant document or signer to which the name/value information will apply.

A useful status value is code '100'. This code is sent when a document is
available for download. Use this code rather than (or in addition to ) status '30'
to confirm the document is available for download. There can be a lag between signing
and document creation so use this code to be sure a document is both signed and ready for download.


Other callback names and values
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Email bounces - 'bounce'

Email bounces are also in the callback. The resource_uri refers to the relevant signer. The name is 'bounce' and the value indicates the nature of the
error.

Storage retrievals - 'storage_retrieval'

After a while signed PDFs are put in long term storage. It can take 4 hours or so to retrieve
these documents. If you attempt to download a document in storage you will receive a 202 HTTP
code from the /pdf/ endpoint. When the document is available you will be notified in the callback. The
resource_uri will be the relevant document reference, name will be 'storage_retrieval' and value
will be 'completed'. You will of course never need this since you will have downloaded and backed-up your
documents before we moved it into long-term storage, right..?


N.B. your code should anticipate
that the callback could be used further forms of
notification and that different strings in 'name' and 'value' could be added at a later date.




Validate
~~~~~~~~~~~~

You can use the 'signed' string to verify the message comes from us by using (i) the
sent data (make sure you encode it into a JSON string),
(ii) the signed string and (iii) our public X509 certificate::

    -----BEGIN CERTIFICATE-----
    MIIFSjCCAzICAQAwDQYJKoZIhvcNAQEFBQAwazEeMBwGA1UEBxMVRWRpbmJ1cmdo
    LFNjb3RsYW5kLEdCMRswGQYDVQQKExJMZWdhbGVzaWduIExpbWl0ZWQxLDAqBgNV
    BAMUI2xlZ2FsZXNpZ24uY29tL2VtYWlsQGxlZ2FsZXNpZ24uY29tMB4XDTEzMDIw
    MTE2MjgyN1oXDTMzMDIwMTE2MjgyN1owazEeMBwGA1UEBxMVRWRpbmJ1cmdoLFNj
    b3RsYW5kLEdCMRswGQYDVQQKExJMZWdhbGVzaWduIExpbWl0ZWQxLDAqBgNVBAMU
    I2xlZ2FsZXNpZ24uY29tL2VtYWlsQGxlZ2FsZXNpZ24uY29tMIICIjANBgkqhkiG
    9w0BAQEFAAOCAg8AMIICCgKCAgEAyzOOqZJ0BnwhJyOSBj1P3W7EGouKe0M1KFif
    CEm9jlw1q5iKupOnbqJRAKG+znDTeocFfwywnQnz7b6AfZ33QiR/HYFJT4J+3HCO
    186VUNsXcK3KtJxfIhsO03zFfIaoRmXLkwaUJlP2dfGc10PNX6EBYyqzRG6TzlB6
    uNdb3R/EeSTMn+VVyZ8E/L1ujeAFW8nZ+V8xAdvqSSeP3tMHwcRq1u6Mk9DUdlhs
    dL//sdTEkNar3FJAlisvXlLs6deXSLsIfr8g6K/59g0HsCEvRKqVa0WFs4OpPR3q
    dN+0PecON0pU6tRuN+9TTsss3hzjRfijiiktLaAIKZrupZDPZ9gSb5bfwL6zjdTN
    y3TTb/SzNZ1agwRoqftOAfff9ffCNUSrtQr4g3isGgtj2jQQ/W7+yFvEGGqy9/Co
    inlaNJqJPYdfYhwJ30sihTkKKzIcUKRGvrnW7QlIl+6+QMToCGql1AbVG1CIQeiP
    NkAmsQxieGrE0OszG1mTx1+SmBGo7WV32iV6sjmaOGHdE9zeNjRSJRS/SIWY8V+b
    2i2W7zi3DvziRPqg6LUJX2pgAuDUmTxg6gMEzh8HHT36igzainrkKXqqoBsqwttY
    OikzGBBcctlhyWApgNqKi953S6dvEMsjWuyzqaYTn4QkQZnfZaIHj8KVQOI3cCgD
    6m8BjTMCAwEAATANBgkqhkiG9w0BAQUFAAOCAgEARHEUsvB7aa+/hp1dR6bqaR0W
    bigK3Xm8LLB7PR0AjojOW1TUUDQLFKkHayqMNB0jcML/7Np4dGhOuAB6hfaq09hw
    xLexhix9DXgnm89QPzClHDTHzcbB2sqhqZKRlsJXfW3Dsh3iN9QdWt341ZsmaPBi
    gPvJpzKCoG4FhUsXUwCKERh41xssPsg7c417+OnU15I63bb4SlGH9+6j4kHGZh7p
    2Sxn27CC4tDLGkI6fOVZapmN1iM+x17n/GCOTDrqlGXw72u0xH3yUA8W75HyuL31
    m/6jlO9VQfY7ZVJn9IuoFjX+sU3uTz7PZhqFHLi2M4hVSaRgJ5JZBkUtr2eTvvhL
    iPBwjOfdba4rulqVni7joN1k4ulYnrpf6jmC/sbUPQkU9HbAUOhibyLEkysEhUPb
    78aFbM+4c6rhzSRL5gvN7lVmBTZimmIs0x5y+Q5+GSIrRZgHkSxOGkrQtdsrJStg
    zerSTb5dF+L9Jv0d2AhIWMLP4KIO4NMj/ljiPOwtqPHU2KSsRivJA1xybFI4peBE
    aAocb936W/RabYdVCPITLsYA4ThApMI+jpJPvG8JKTrauOwZRBOq82lBJuoBgkAp
    h6A3SPeR5GCnCRmPIvrv21Q7dm7Gg4lbPtEemH3QdNlCIY1BzADD/H1zDAU+0J5P
    +PUQCjWRM3JmFbuYTjs=
    -----END CERTIFICATE-----


The language you're using should have PKI library you can use to validate
the signature using those three pieces of information.

    

/sandbox/  -  Option 1 sandbox
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


While you're in sandbox mode you can create fake notifications to send to your callback url.

Send an authorized GET request to the /sandbox/ endpoint,
and add 'resource_uri' and 'value' to
the querystring.

'resource_uri' can be any resource_uri string and value
should be an integer. A typical resource_uri might look like '/api/v1/document//4a315cfe-1731-484c-9d3b-b10b68168d42/
and a value might be '30'.

The callback URL could receive either a document or a signer resource_uri to indicate a change
in status for either of those. For possible status codes go to: :ref:`Document status codes <document_status_codes>` 


Option 2. Realtime update when a document is signed
====================================================


Whenever a document is signed you'll get a POST with a JSON dictionary containing the resource URI of the
document that has been signed and the group that it belongs to. That's it. :: 

    {"resource_uri": "/api/v1/document/[id-of-doc]/", "group": "/api/v1/group/[group-url-name]/", "name": "name of doc", "uuid": "[id-of-doc]"}


(If you used the API to create the document in the first place, the status resource_uri of the document is included in the 'Location'
header of the 201 'created' response.  Just replace '/status/' with '/document/' to get the document resource_uri.)


Final note
~~~~~~~~~~~~~~~

Warning - if your callback URL fails to respond the callbacks will be suspended, so please
contact us when you've tested and confirmed your URL is accessible.
