
Background information
==========================================


Methods
~~~~~~~~~~~~~~~~~~~~~~~~~~

Each resource should list the methods available.

* GET - read only
* POST - creates a new object
* PATCH - edit an existing object, requires only fields being changed
* DELETE - remove an object permanently


POST will return 'Location' header with detail endpoint for the new object



Time format
~~~~~~~~~~~~~~~~~~~~~~~~~~

All times are in UTC. Most objects have 'created' and 'modified' attributes, a datetime is formatted as::

    YYYY-mm-ddTH:M:S


HTTP response codes
~~~~~~~~~~~~~~~~~~~~~~~~~~

* 400 - bad request (usually a failed POST)
* 401 - unauthorized
* 405 - method not allowed
* 429 - throttled
* POST & 201 - successful
* PATCH &  202 - successful
* DELETE & 204 - successful


User attribute
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The requirement for a user when sending data to the server looks redundant since you can
only send the API user, and some calls it can be omitted.
However we'll be extending this attribute in due course so the API user
will be able to post on behalf of others members of the group(s). Coming soon...


.. _general_fields:

Fields
~~~~~~~~

Fields are parts of the document you want a signer to complete before they sign.
You can manipulate fields in your text by adding SPAN elements with a set of special attributes.
Legalesign will do the rest.

The easiest way to familiarise yourself with fields is to create a template using different
field types in the web interface and inspect the HTML. The attributes are:

============== ======= ======================================================================================
SPAN Attribute Type    Description
============== ======= ======================================================================================
class          string  must always have the value 'field'
data-signee    integer the signer to which the field applies (start from 1) - see note below
data-optional  bool    true/false - require the field to be completed by the signer
data-type      string  can be 'text','select' or 'signature', text is a plain text field and select is a list
data-options   string  if using 'select' data-type, add the drop down options delimited with '^', e.g. yes^no
============== ======= ======================================================================================

data-type 'signature' note: add this where you want to place a signature. If you use this data-type make sure you
have at least one for each signer. If you do not use this data-type check the 'signature_placement' document
option for adding signatures automatically.

data-signee note: confusingly the signer order is zero-indexed in the API calls, but 1-indexed in the text fields. Thus signer with order '0'
refers to 'data-signee=1' field. Sorry about that.

If you want to add/edit templates and document text in your web site you may find it helpful to incorporate the
tinymce fields plugin at https://bitbucket.org/legalesign/tinymce_field.


Images
~~~~~~~~~~~~~~~~~~~~~~

You can add img tags to your html in the main document, header or footer. Images must (a) be linked from
a secure HTTPS server, you should *not* use HTTP, (b) and they *must* have width and height attributes.


