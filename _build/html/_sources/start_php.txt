
Quick start - code samples
==========================================

PHP
~~~~~

Get group details, send a html doc and send a pdf doc. Uses the Guzzle PHP REST API client (http://guzzlephp.org/)::


    <?php
    //Accessing Legalesign API using the Guzzle PHP REST API client
    //http://guzzlephp.org/, follow install instructions at: https://github.com/guzzle/guzzle
    
    require_once 'vendor/autoload.php';
    use Guzzle\Http\Client;
    
    $root_url = 'https://legalesign.com/api/v1/';
    $auth_header = 'ApiKey f6009234a674463d8d2def328b4321:a7f0fcbe8bc72ddd31bc5786c6f629d0aef1eeb0';
    $headers = array('Authorization'=>$auth_header, 'Content-Type'=>'application/json');
    
    
    $client = new Client($root_url);
    $client->setDefaultHeaders($headers);
    
    //EXAMPLE OF GET - GET THIS USERS GROUPS
    
    $request = $client->get('group/');
    $response = $request->send()->json();
    print_r($response['objects']);
    //Array ( [0] => Array ( [created] => 2013-04-24T08:55:30 [is_active] => 1
    //[modified] => 2013-04-24T08:55:30 [name] => My Group
    //[resource_uri] => /api/v1/group/my-group/
    //[slug] => my-group [user] => /api/v1/user/5175175b667b4e88ad41be6d2fd322/ ) )
    
    
    //EXMPLE OF POST - SEND A DOCUMENT
    
    //array of signers, note arrays within array
    $signers_post = array(
                          array(
                                'firstname'=>'Joe',
                                'lastname'=>'Bloggs',
                                'email'=>'email@legalesign.com',
                                'order'=>0,
                               )
    );
    //doc variables
    $doc_post = array(
                      'group'=>'/api/v1/group/my-grooup/',
                      'name'=>'Name of doc',
                      'text'=>'<h1>Draft doc</h1><p>etc..</p>',
                      'signers'=>$signers_post,
                      'do_email'=>True,
                      );
    
    //encode in json and post
    $request = $client->post('document/', null, json_encode($doc_post));
    $response = $request->send();
    
    //new docs resource_uri is contained in 'Location' header
    $doc_resource_uri = $response->getHeader('Location');
    echo $doc_resource_uri;
    //http://legalesign.com/api/v1/status/fab190f1-7019-41f4-bb14-3c86067d7169/
    
    //use the /status/ endpoint for an update on status or /document/ endpoint for all the docs information
    $request = $client->get('status/fab190f1-7019-41f4-bb14-3c86067d7169/');
    $response = $request->send()->json();
    print_r($response);
    //Array ( [archived] => [resource_uri] => /api/v1/status/fab190f1-7019-41f4-bb14-3c86067d7169/ [status] => 10 )
    
    //when status is 30, the document is signed. we can download it.
    //either poll for status or use the notification feature
    
    
    //Send a PDF doc that has been set up on Legalesign
    //with the text for sections that need to be completed by sender
    
    //for sender text, create array with the text section labels for keys
    $sender_text= array(
        'label 1' => 'first value',
        'label 2'=>'another value',
    )
    
    $doc_post = array(
                      'group'=>'/api/v1/group/my-grooup/',
                      'name'=>'Name of doc',
                      'templatepdf'=>'/api/v1/templatepdf/<id>/',
                      'signers'=>$signers_post,
                      'do_email'=>True,
                      'pdftext'=>$sender_text,
                      )
    
    $request = $client->post('document/', null, json_encode($doc_post));
    $response = $request->send();
    
    ?>


Python
~~~~~~~

Send a HTML doc and a PDF doc in python using slumber (http://slumber.readthedocs.org/en/v0.6.0/)::


    import slumber
    from requests import session
    
    username = '<username>'
    api_key = '<key>'
    url = 'https://legalesign.com/api/v1/document/'
    
    sess = session()
    sess.headers.update({'Authorization': 'ApiKey %s:%s' % (username, api_key) })
    api = slumber.API(url, sess)
    
    resp = api.status.get()
    print resp
    
    //signers info - note signer dictionary is within a list
    
    signers = [
        {
            'firstname': 'Joe',
            'lastname': 'Bloggs',
            'email': 'joe@example.com',
            'order': 0,
        }
    ]
    
    //Send HTML directly
    
    data_post = {
        'name': 'first test doc',
        'group': 'api/v1/<my-group>/',
        'do_email': True,
        'signers': signers,    
        'text': '<h1>Sample doc</h1><p>Text of sample doc ...etc</p>',  
    }
    
    resp = api.document.post(data_post)
    print resp
    'OK'


    //Send a PDF doc that has been set up on Legalesign
    //with the text for sections that need to be completed by sender
    
    //for sender text, create array with the text section labels for keys
    sender_text = {
        'label 1': 'First value',
        'label 2': 'Second value
    }
    
    data_post = {
        'name': 'first test doc',
        'group': 'api/v1/<my-group>/',
        'do_email': True,
        'signers': signers,
        'templatepdf': '/api/v1/templatepdf/<id>',
        'pdftext': sender_text
    }
    
    resp = api.document.post(data_post)
    print resp
    'OK'

Ruby
~~~~~~~


Send a HTML doc and a PDF doc in ruby using httparty (https://github.com/jnunemaker/httparty)::

    require 'httparty'
    
    username = '<username>'
    key = '<key>'
    url = 'https://legalesign.com/api/v1/document/'
    
    header = {'Authorization' => 'ApiKey '+username+':'+key, 'Content-Type' => 'application/json'}
    
    signers_data = [
        { 'firstname' => 'Joe', 'lastname' => 'Bloggs', 'email' => 'joe@example.com', 'order' => 0}
    ]
    
    //SendHTML directly
    
    post_data  = {
        'name' => 'Test doc ruby',
        'group' => '/api/v1/group/<mygroup>/',
        'text' => '<h1>Sample doc</h1><p>some text for sample doc</p>',
        'do_email'=> true,
        'signers'=> signers_data,
    }
    
    resp = HTTParty.post('https://legalesign.com/api/v1/document/', :body=>post_data.to_json, :headers=>auth_headers)
    resp.parsed_response
    "OK"
    
    
    //Send a PDF doc that has been set up on Legalesign
    //with the text for sections that need to be completed by sender
    
    //for sender text, create array with the text section labels for keys
    sender_text = {
        'label 1'=> 'First value',
        'label 2'=> 'Second value
    }
    
    data_post = {
        'name' => 'first test doc',
        'group' => 'api/v1/<my-group>/',
        'do_email' => True,
        'signers' => signers,
        'templatepdf' => '/api/v1/templatepdf/<id>',
        'pdftext' => sender_text
    }
    
    resp = HTTParty.post('https://legalesign.com/api/v1/document/', :body=>post_data.to_json, :headers=>auth_headers)
    resp.parsed_response
    "OK"